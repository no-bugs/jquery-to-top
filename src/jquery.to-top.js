/*! jquery scrollTop v1.0.0 25.08.2019
 * 
 *
 * Copyright (c) 2019 No Bugs;
 * Licensed under the MIT license */
(function ( $ ) {
    $.fn.toTop = function(options) {
        var $items = this;
        var _options = {
            'speed': 400
        };
        _options = $.extend({}, _options, options);
        /** кнопка наверх */
        $(window).scroll(function () {
            if ($(this).scrollTop() > 0) {
                $items.fadeIn();
            } else {
                $items.fadeOut();
            }
        });
        $items.click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, _options.speed);
            return false;
        });
    };
})(jQuery);
